-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2015 at 01:23 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `carsdatabase`
--
CREATE DATABASE IF NOT EXISTS `carsdatabase` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `carsdatabase`;

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE IF NOT EXISTS `cars` (
  `CAR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CAR_NAME` char(20) NOT NULL,
  `CAR_COLOR` char(20) NOT NULL,
  PRIMARY KEY (`CAR_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`CAR_ID`, `CAR_NAME`, `CAR_COLOR`) VALUES
(1, 'A', 'BLUE'),
(2, 'B', 'BLUE'),
(3, 'C', 'BLUE'),
(4, 'D', 'RED'),
(5, 'E', 'BLUE'),
(6, 'F', 'BLUE'),
(7, 'F', 'RED'),
(8, 'G', 'BLUE');

-- --------------------------------------------------------

--
-- Table structure for table `carsorder`
--

CREATE TABLE IF NOT EXISTS `carsorder` (
  `CARS_POSITION` varchar(10) NOT NULL,
  `CARS_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carsorder`
--

INSERT INTO `carsorder` (`CARS_POSITION`, `CARS_ID`) VALUES
('0.3', 1),
('0.6', 2),
('0.2', 3),
('0.7', 4),
('0.5', 5),
('0.4', 6),
('7', 7),
('8', 8);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
