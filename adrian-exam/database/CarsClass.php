<?php
require("database/config.php");

class CarsClass {
	public $db;
	
	public function __construct($pdo){
		$this->db = $pdo;
	}
	
	private function runQuery($sql){
		if($this->db->query($sql)){
			return 0;
		} else {
			return 1;
		}
	}
	
	public function saveNewCar($name, $color){
		$sql = "INSERT INTO `cars`(CAR_NAME, CAR_COLOR) VALUES('{$name}','{$color}')";
		if($this->db->query($sql)){		
			$sql2 = "INSERT INTO `carsorder`(CARS_ID, CARS_POSITION) VALUES('{$this->db->lastInsertId()}','{$this->db->lastInsertId()}')";
			
			if($this->db->query($sql2)){		
				return 0;
			} else {
				return 1;
			}
		} else {
			return 1;
		}
	}
	
	public function moveCar($movecar, $moveto, $carid){
		// get id and position
		$sql = "SELECT `CARS_POSITION` FROM `carsorder` WHERE `CARS_ID` = '{$moveto}' LIMIT 1";
		$position = "";
		foreach($this->db->query($sql) as $row){
			$position = $row['CARS_POSITION'];
		}
		
		$decimaldigit = pow(10, strlen(substr(strrchr($position, "."), 1)));
		
		if((1/$decimaldigit) == 1){
			$placement = 0.1;
		} else {
			$placement = 1/$decimaldigit;
		}
		
		// check for same position id
		$sameid = 1;
		while($sameid == 1){
			$sql2 = "SELECT COUNT(*) FROM `carsorder` WHERE `CARS_POSITION` = '{$position}' LIMIT 1";
			foreach($this->db->query($sql2) as $row){
				$sameid = $row['COUNT(*)'];
			}
			
			if($sameid > 0){
				if($movecar == "ABOVE") {
					$position = $position-$placement;
				} else if($movecar == "BELOW") {
					$position = $position+$placement;
				}			
			}
			
		
		}
		
		
		$sql3 = "UPDATE `carsorder` SET `CARS_POSITION`='{$position}' WHERE `CARS_ID` = {$carid}";
		return $this->runQuery($sql3);
	}
	
	public function getAllCarData($sort){
		$cardata = array();
		
		if($sort == "blue") {
			$sort = " WHERE `CAR_COLOR` = 'BLUE' ";
		} else if($sort == "red") {
			$sort = " WHERE `CAR_COLOR` = 'RED' ";
		} 
		
		foreach($this->db->query("SELECT * FROM `cars` LEFT JOIN `carsorder` ON `cars`.`CAR_ID` = `carsorder`.`CARS_ID` {$sort} ORDER BY `carsorder`.`CARS_POSITION` ASC" ) as $row) {
			array_push($cardata, $row);
		}
		
		return $cardata;
	}
}

?>