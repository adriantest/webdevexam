<?php
require("database/CarsClass.php");
$cars = new CarsClass($pdo);

// get all data
$sort = "";
if(isset($_GET['sort'])){
	$sort = $_GET['sort'];
}
$data = $cars->getAllCarData($sort);


// CATCH GET
$car_action = "";

if(isset($_GET['saved'])){
	
	if($_GET['saved'] == "true"){
		$car_action = "INSERTED NEW CAR!";
	} else {
		$car_action = "SOMETHING HAPPENED!";
	}
}

if(isset($_GET['moved'])){
	if($_GET['moved'] == "true"){
		$car_action = "MOVED A CAR!";
	} else {
		$car_action = "SOMETHING HAPPENED!";
	} 
}

// new car
if(isset($_GET['car_name'])){
	if($cars->saveNewCar($_GET['car_name'], $_GET['car_color']) == 0){
		header("Location: index.php?saved=true");
	} else {
		header("Location: index.php?saved=false");
	}
}

// move car
if(isset($_GET['move'])){
	if($cars->moveCar($_GET['movecar'], $_GET['moveto'], $_GET['move']) == 0){
		header("Location: index.php?moved=true");
	} else {
		header("Location: index.php?moved=false");
	}
}

?>


<html>
<head>
	<title>CARS exam</title>
	<style>
	html {
		font-family : Verdana;
	}
	</style>
</head>
<body>
	<?php
	if(!isset($_GET['edit'])){
	?>
	<h2>ADD A CAR</h2>
	<?php echo $car_action; ?>
	<form action="index.php" method="GET">
		<label>CAR NAME:</label>
		<input type="text" name="car_name" /> 
		
		<label>CAR COLOR:</label>
		<select name="car_color">
			<option value="BLUE">BLUE</option>
			<option value="RED">RED</option>
		</select>
		<input type="submit" value="SUBMIT" />
	</form>
	
	<?php
	} else {
	?>
	<h2>MOVING CAR</h2>
	<form action="index.php" method="GET">
		<input type="hidden" name="move" value="<?=$_GET['id']?>" />
		<label>MOVE CAR:</label>
		<input type="text" size="5" value="<?=$_GET['carname']?>" disabled /> 
		
		<select name="movecar">
			<option value="ABOVE">ABOVE</option>
			<option value="BELOW">BELOW</option>
		</select>
		
		<label>CAR:</label>
		<select name="moveto">
			<?php
			foreach($data as $value){
				echo "<option value='{$value['CAR_ID']}'>{$value['CAR_NAME']}</option>";
			}
			?>
		</select>
		
		
		<input type="submit" value="SUBMIT" />
		<a href="index.php">CANCEL</a>
	</form>
	<?php
	}
	?>
	
	
	<p><a href="index.php?sort=blue">SHOW BLUE CARS</a> | <a href="index.php?sort=red">SHOW RED CARS</a> | <a href="index.php">SHOW ALL CARS</a></p>
	<table border="1">
		<tr>
			<th>NAME</th>
			<th>COLOR</th>
			<th>ACTION</th>
		</tr>
		<?php
		foreach($data as $value){
			echo "<tr>
					<td>{$value['CAR_NAME']}</td>
					<td>{$value['CAR_COLOR']}</td>
					<td><a href='?edit=true&carname={$value['CAR_NAME']}&carcolor={$value['CAR_COLOR']}&id={$value['CAR_ID']}'>MOVE</a></td>
				  </tr>";
		}
		?>
	</table>
</body>
</html>